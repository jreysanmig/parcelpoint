package com.parcelpoint.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdidasReturnDetailsPage extends BasePage {

	By orderId = By.id("order-number-returns");
	By reasonForReturn = By.id("retailerReturnsReasonsId");
	By name = By.id("delivery-name-createAccount");
	By email = By.id("delivery-email-createAccount");
	By mobile = By.id("delivery-mobile-createAccount");

	By nextButton = By.id("submitReturnsForm");

	public AdidasReturnDetailsPage(WebDriver driver) {
		super(driver);
	}

	public void enterOrderId(String value) {
		enterText(orderId, value);
	}

	public void selectReasonForReturn(String value) {
		selectFromDropdown(reasonForReturn, value);
	}

	public void enterName(String value) {
		enterText(name, value);
	}

	public void enterEmail(String value) {
		enterText(email, value);
	}

	public void enterMobile(String value) {
		enterText(mobile, value);
	}

	public void clickNext() {
		clickOn(nextButton);
	}

}
