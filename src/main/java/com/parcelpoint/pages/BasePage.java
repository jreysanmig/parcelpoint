package com.parcelpoint.pages;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class BasePage {
	
	WebDriver driver;
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
	}
	
	protected void open(String url) {
		driver.get(url);
	}
	
	protected void enterText(By elementLocation, String value) {
		driver.findElement(elementLocation).sendKeys(value);
	}
	
	protected void clickOn(By elementLocation) {
		driver.findElement(elementLocation).click();
	}
	
	protected void chooseRadio(By elementLocation, String value) {
		List<WebElement> radios = driver.findElements(elementLocation);
		for(WebElement option : radios) {
			if(value.equals(option.getAttribute("value"))) {
				option.click();
			}
		}
	}
	
	protected void chooseCheckbox(By elementLocation, String ... values) {
		List<WebElement> checkboxes = driver.findElements(elementLocation);
		for(WebElement option : checkboxes) {
			if(Arrays.asList(values).contains(option.getAttribute("value").trim())) {
				option.click();
			}
		}
	}
	
	protected void selectFromDropdown(By elementLocation, String value) {
		Select dropdown = new Select(driver.findElement(elementLocation));
		dropdown.selectByVisibleText(value);
	}
	
	protected String getText(By elementLocation) {
		return driver.findElement(elementLocation).getText();
	}
	
	protected boolean isDisplayed(By elementLocation) {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		boolean isDisplayed = false;
		try {
			isDisplayed = driver.findElement(elementLocation).isDisplayed();
		} catch (NoSuchElementException e) {
			isDisplayed = false;
		}
		
		return isDisplayed;
	}

}
