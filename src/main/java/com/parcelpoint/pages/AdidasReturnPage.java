package com.parcelpoint.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdidasReturnPage extends BasePage {
	private final String URL = "https://staging.parcelpoint.com.au/adidas";
	
	String storesFrameId = "ppIframeWidget-parcelpoint-stores-widget";
	
	By parcelPoints = By.cssSelector("li.store-list-info");
	By parcelPointName = By.cssSelector("div.title");
	By parcelPointSelect = By.cssSelector("div.select");
	By nextButton = By.id("parcelPointSubmitButton");

	public AdidasReturnPage(WebDriver driver) {
		super(driver);
	}
	
	public void open() {
		super.open(URL);
	}
	
	public void chooseParcelPoint(String name) {
		driver.switchTo().frame(storesFrameId);
		List<WebElement> ppList = driver.findElements(parcelPoints);
		for(WebElement pp : ppList) {
			String storeName = pp.findElement(parcelPointName).getText();
			if(storeName!=null && storeName.contains(name)) {
				pp.findElement(parcelPointSelect).click();
			}
		}
		driver.switchTo().defaultContent();
	}
	
	public void clickNext() {
		clickOn(nextButton);
	}
	

}
