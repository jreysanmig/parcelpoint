package com.parcelpoint.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdidasReturnCompletePage extends BasePage {

	By successMessage = By.cssSelector("p.lead");
	By orderIdLine = By.xpath("//h3[contains(text(),'Return Details')]/following-sibling::*[1]");
	By returnReasonLine = By.xpath("//h3[contains(text(),'Return Details')]/following-sibling::*[3]");

	public AdidasReturnCompletePage(WebDriver driver) {
		super(driver);
	}

	public String getSuccessMessage() {
		return getText(successMessage);
	}

	public String getOrderIdLine() {
		return getText(orderIdLine);
	}
	
	public String getReturnReasonLine() {
		return getText(returnReasonLine);
	}
	
}
