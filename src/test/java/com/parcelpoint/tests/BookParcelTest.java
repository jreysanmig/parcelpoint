package com.parcelpoint.tests;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.parcelpoint.pages.AdidasReturnCompletePage;
import com.parcelpoint.pages.AdidasReturnDetailsPage;
import com.parcelpoint.pages.AdidasReturnPage;

public class BookParcelTest extends BaseTest{
	
	
	@Test
	public void bookParcelSuccessTest( ) {
		String parcelPoint = "Kings Wharf Supermarket";
		String orderId = "AAU12345678";
		String returnReason = "Change of mind (6)";
		String name = "Johnrey";
		String email = "johnreysanmiguel"+(int)(1000*Math.random())+"@gmail.com";
		String mobile = "0431429325";
		
		
		AdidasReturnPage adidasReturnPage = new AdidasReturnPage(driver);
		adidasReturnPage.open();
		adidasReturnPage.chooseParcelPoint(parcelPoint);
		adidasReturnPage.clickNext();
		
		AdidasReturnDetailsPage adidasDetailsPage = new AdidasReturnDetailsPage(driver);
		adidasDetailsPage.enterOrderId(orderId);
		adidasDetailsPage.selectReasonForReturn(returnReason);
		adidasDetailsPage.enterName(name);
		adidasDetailsPage.enterEmail(email);
		adidasDetailsPage.enterMobile(mobile);
		adidasDetailsPage.clickNext();
		
		AdidasReturnCompletePage adidasReturnCompletePage = new AdidasReturnCompletePage(driver);
		assertThat(adidasReturnCompletePage.getSuccessMessage())
			.contains(name)
			.contains("your return has been booked!");
		assertThat(adidasReturnCompletePage.getOrderIdLine()).contains(orderId);
		assertThat(adidasReturnCompletePage.getReturnReasonLine()).contains(returnReason);
		
		
	}
	
	
	
}
