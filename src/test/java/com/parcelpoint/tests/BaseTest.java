package com.parcelpoint.tests;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BaseTest {
	
	static WebDriver driver;
	
	@BeforeClass
	public static void setup() {
		Properties props = System.getProperties();
		try {
			props.load(new FileInputStream("src/main/resources/properties/test.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.setProperties(props);
		String browser = System.getProperty("webdriver.driver");
		switch (browser) {
			case "chrome":
				driver = new ChromeDriver();
				break;
			case "ie":
				driver = new InternetExplorerDriver();
				break;
			case "firefox":
				driver = new FirefoxDriver();
				break;

		}
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	@AfterClass
	public static void cleanUp() {
		driver.quit();
	}
	

}
